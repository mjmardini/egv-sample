function setup() {
	createCanvas(windowWidth, windowHeight);
	background(255, 255, 255);

	// translucent stroke using alpha value
	stroke(0, 0, 0, 15);
}

function windowResized() {
	resizeCanvas(windowWidth, windowHeight)
}

function draw() {
	// draw two random chords each frame
	randomChord();
	randomChord();
}

function randomChord() {
	// find a random point on a circle
	let angle1 = random(0, 2 * PI);
	let xpos1 = windowWidth/2 + windowWidth/2 * cos(angle1);
	let ypos1 = windowHeight/2 + windowHeight/2 * sin(angle1);

	// find another random point on the circle
	let angle2 = random(0, 2 * PI);
	let xpos2 = windowWidth/2 + windowWidth/2 * cos(angle2);
	let ypos2 = windowHeight/2 + windowHeight/2 * sin(angle2);

	// draw a line between them
	line(xpos1, ypos1, xpos2, ypos2);
}