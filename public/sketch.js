var x, y, xspeed=3, yspeed=3, bg=255
var img
function preload(){
	img = loadImage('egv.png')
}
function setup(){
	createCanvas(windowWidth,windowHeight)
	x=random(100,windowWidth-100)
	y=random(100,windowHeight-100)
}
function windowResized(){
	resizeCanvas(windowWidth,windowHeight)
}
function draw(){
	background(bg,50)
	x+=xspeed;y+=yspeed;
	// fill(37,41,60)
	// circle(x,y,20,20)
	image(img, x, y)
	if(x+90 > windowWidth || x < 0) xspeed*=-1
	if(y+51 > windowHeight || y < 0) yspeed*=-1
	if(x > windowWidth) x=windowWidth-20
	if(y > windowHeight) y=windowHeight-20
}